Backup & Migrate Dropbox
========================
This module provides Dropbox as a destination to "Backup & Migrate", allowing to
back up your site's data and files offsite into the cloud.

It allows you to:
- Define Dropbox destinations.
- Manually backup to Dropbox.
- Define schedules using a Dropbox destination to automatically backup to
  Dropbox.
- See a list of saved backups in your Dropbox destination.
- Restore from a backup in your Dropbox destination.
- Delete backups saved in your Dropbox destination.

Requirements
------------
- PHP 5.6 or later.
- Backup and Migrate 7.x-3.0 or later.

Installation
------------
As usual. After enabling the module you can define (a) Dropbox destination(s)
and define schedules using this/those destinations.

Updating (from 7.x-2.x)
-----------------------
Version 7.x-3.0 completely changed the way that authorization is obtained to
access your Dropbox.

**How it was (7.x-2.x)**: You had to create a (private) Dropbox App yourself
and generate a long-lived authorization token for it in your Dropbox App
console. This is not the recommended way - not having to create your own App,
nor using a long-lived authorization token, and will be
<a href="https://dropbox.tech/developers/migrating-app-permissions-and-access-tokens#retiring-legacy-tokens">
retired by Dropbox mid 2021</a>.



**How it is (7.x-3.x)**: We created a Drupal Backup & Migrate Dropbox App and
you have to authorize this new App to access (a sub folder specific for this
App on) your Dropbox account. This is far easier for you, as you no longer have
to go to the Dropbox App console to create a private App for yourself. This
version thus contains a completely rewritten authorization part that now uses
the OAuth2 with PKCE scheme to obtain short-lived bearer tokens.

**Note**: Because we now use a new Dropbox App you are strongly advised to
delete the App you created to get the earlier versions of this module to work.

Defining a Dropbox destination
------------------------------
1. Go to "Configuration - System - Backup and Migrate - Settings -
   Destinations - Add destination".
2. Select 'Dropbox' from the off-site destinations list.
3. Fill in a 'Destination name'.
4. Click on the button 'Get an authorization code on Dropbox'. This will bring
   you to the Dropbox site in a new tab.
5. If you get a warning titled 'Before you connect this app...', click on
   'Continue'.
6. Read abut the permissions that the 'Drupal Backup & Migrate' App needs and
   click on 'Allow'. Note that these are the minimal permissions this App needs:
   it can only read & write in its **own** sub folder within your Dropbox
   account.
7. Dropbox will present you with an 'Access Code', click on it (this will
   select it) and copy it.   
4. Go back to the 'add(/edit) destination' page on your Drupal site and paste
   the access code into the field (below the button).
5. Optionally fill in the name of a sub folder to place the backups to this
   destination in. E.g. use this if you have multiple destinations for this site
   (daily, weekly, ...) or multiple sites that backup to this account (mysite1,
   mysite2/daily, mysite2/weekly, ...). Upon saving, the module will create the
   sub folder in the Apps folder in your Dropbox account.
6. Click on "Save destination".
7. The module will test if everything works fine, i.e. if the given
   authorization works correctly.
8. Start using the destination in your schedules and manual backups.   

Troubleshooting
---------------
- If you receive out of memory errors use either Drush or the variables module
  to set the 'backup_migrate_dropbox_upload_limit' variable. Set the variable as
  high as possible without receiving an out of memory errors to ensure maximum
  performance.
- If you receive a 4XX error check that the access token is correct or generate
  a new token and copy it over.
- If you receive a 5XX error check status.dropbox.com to insure that the server
  is up and running.
- If you receive a different error please create an issue on the Backup Migrate
  Dropbox module page at https://www.drupal.org/project/backup_migrate_dropbox.
